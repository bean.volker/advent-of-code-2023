# def without_words():
#     file = open('day1.txt', 'r')
#     i = 0
#     final_tally = 0
#     while True:
#         first_num = 0
#         second_num = 0
#         two_digit = 0
#         line = file.readline()
#         if not line:
#             break

#         line_list = list(line)
#         line_list_reversed = line_list.copy()
#         line_list_reversed.reverse()
#         for char1 in line_list_reversed:
#             if char1.isdigit():
#                 first_num = char1

#         for char2 in line_list:
#             if char2.isdigit():
#                 second_num = char2
        
#         two_digit = int(str(first_num) + str(second_num))
#         final_tally += two_digit
#         i += 1



#         print(final_tally)


def two_digit(list):

    first_num = str(list[0])
    second_num = str(list[-1])
    two_digit = first_num + second_num
    two_digit = int(two_digit)

    return two_digit






def convert_to_num(num):
    if num == 'one':
        num = '1'
    elif num == 'two':
        num = '2'
    elif num == 'three':
        num = '3'
    elif num == 'four':
        num = '4'
    elif num == 'five':
        num = '5'
    elif num == 'six':
        num = '6'
    elif num == 'seven':
        num = '7'
    elif num == 'eight':
        num = '8'
    elif num == 'nine':
        num = '9'
    return str(num)




def with_words():

    total = 0
    file = open('day1.txt', 'r')
    line_num = 0

    while True:
        line = file.readline()
        line = line.lower()
        line = [*line]
        new_list = list()
        if not line:
            break

        line_num += 1
        
        num_list = ['one', 'two', 'three','four','five', 'six', 'seven', 'eight', 'nine']


        i = 0
        j = 0
        k = 0

        line_len = len(line)

        while i < line_len:
            if line[i].isdigit():
                new_list.append(line[i])
                i += 1
            else:
                first_i = i
                num_len = len(num_list[j])
                while j < len(num_list) and k < num_len:
                    if num_list[j][k] != line[i]:
                        j += 1
                        k = 0
                        i = first_i
                    elif num_list[j][k] == line[i]:
                        if k == 1:
                            num_len = len(num_list[j])
                        i += 1
                        k += 1
                if k >= num_len:
                    num = convert_to_num(num_list[j])
                    new_list.append(num)
                    i = first_i + 1
                    k = 0
                    j = 0
                elif k < num_len:
                    i += 1
                    j = 0
                    k = 0
                


        two_digit_num = two_digit(new_list)
        total += two_digit_num
        print('line_num', line_num)
        print('two_digit', two_digit_num)
        print(total)
        # same_word = True
        # line_len = len(line)
        # num_list_len = len(num_list)
        # num = str()

        # while i < (line_len):
        #     if j < num_list_len:
        #         num_len = len(num_list[j])
            
        #     if j > (num_list_len - 1):
        #         i += 1
        #         j = 0

        #     if line[i].isdigit():
        #         new_list.append(line[i])
        #         i += 1
        #         print(line[i])

        #     else:
        #         while j < (num_list_len):
        #             print('line[i]', line[i])
        #             print(num_list[j][k])
        #             if line[i] == num_list[j][k]:
        #                 first_i = i
        #                 while k < (num_len - 1) and same_word:
        #                     i += 1
        #                     k += 1
        #                     if line[i] != num_list[j][k]:
        #                         same_word = False
        #                         i = first_i
        #                         k = 0

        #                 if same_word:
        #                     num = convert_to_num(num_list[j])
        #                     new_list.append(num)
        #                     j += 1
        #                     i += 1

        #                 else:
        #                     same_word = True
        #                     j += 1
        #             else:
        #                 j += 1
        #         k = 0
        #         #j += 1
        #         #i += 1

    print(total)


            


def main():
    with_words()
main()
