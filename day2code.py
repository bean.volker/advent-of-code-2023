def creating_dict (game_num, gamedict, line_list,listlen):
    i = 0

    game = "Game"
    inner_dict = {}
    inner_dict['red'] = list()
    inner_dict['green'] = list()
    inner_dict['blue'] = list()
    while listlen > i:
        item = line_list[i]
        item = item.replace(",", "")
        item = item.replace(";",'')
        item = item.replace(":","")
        if item == 'red':
            i += 1
            inner_dict['red'].append(int(line_list[i]))
        elif item == 'green':
            i += 1
            inner_dict['green'].append(int(line_list[i]))
        elif item == 'blue':
            i += 1
            inner_dict['blue'].append(int(line_list[i]))

        elif item == 'Game':
            curGame = game + str(game_num)
            gamedict[curGame] = inner_dict
            game_num += 1
        i += 1
    return game_num

def reading_dict_max(gamedict, game_num, games_possible):
    i = 1
    while i < game_num:
        j = 0
        game_possible = True
        red_len = len(gamedict["Game" + str(i)]['red'])
        for num in gamedict["Game" + str(i)]['red']:
            if num > 12:
                game_possible = False
        for num in gamedict["Game" + str(i)]['green']:
            if num > 13:
                game_possible = False
        for num in gamedict["Game" + str(i)]['blue']:
            if num > 14:
                game_possible = False
        if game_possible:
            games_possible += i #adds the ID's of all the games that would 
           
        i += 1                  #have been possible to complete
    return games_possible

def reading_dict_min(gamedict, total_power, game_num):

    i = 1

    while i < game_num:
        print('entered loop')

        gamedict['Game' + str(i)]['red'].sort()
        red_min = gamedict['Game' + str(i)]['red'][-1]

        gamedict['Game' + str(i)]['green'].sort()
        green_min = gamedict['Game' + str(i)]['green'][-1]

        gamedict['Game' + str(i)]['blue'].sort()
        blue_min = gamedict['Game' + str(i)]['blue'][-1]

        total_power += red_min * green_min * blue_min
        print(total_power)
        i += 1

    return total_power

    


def main():
    file = open('day2.txt', 'r')
    i = 0
    gamedict = {}
    game_num = 1
    games_possible = 0
    total_power = 0
    while True:
        line = file.readline()
        if not line:
            break
        line_list = line.split()
        line_list.reverse()
        listlen = len(line_list)
        game_num = creating_dict (game_num, gamedict, line_list,listlen)
    games_possible = reading_dict_max(gamedict, game_num, games_possible)
    total_power = reading_dict_min(gamedict, total_power, game_num)
    print(gamedict)
    print(gamedict['Game1']['red'])
    print(total_power)
    print('end')


main()